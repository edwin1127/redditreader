package us.theappacademy.redditreader;

import android.net.Uri;

import java.net.URI;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}
